=================
matrix-repository
=================

matrix-repository is a collection of matrices with special
properties. These properties include: known inverses and/or known
eigenvalues; ill-conditioned and/or rank deficient matrices; symmetric,
positive definite, orthogonal, defective, involutory, and/or totally
positive.

The documentation can be found `here
<http://matrix-repository.readthedocs.org/en/latest>`_.

Dependencies
============

* gfortran compiler 

* To use the Python interface, you need Python 2.6 or above, 
  include Python 3.x, with Numpy v1.8. 

* To use the module ``access``, you need python-pandas.   

Installation
============

To install matrix-repository, type ::

  $ git clone https://bitbucket.org/matrix-repository/matrix-repository.git
  
then do::
  
  $ cd matrix-repository
  $ make

To install the Python interface of matrix-repository, type ::

  $ make python

 
