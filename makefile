VPATH = src/base  src/util
FC = gfortran
FFLAGS = -Wall -Wextra -fbounds-check -Wimplicit-interface
UTILITY = mr_type.f90 mr_util.f90
UTILITY_OBJ = $(UTILITY:.f90=.o)
SOURCES =  matrix_repository.f90 d_chow.f90 s_chow.f90 d_cauchy.f90 \
           s_cauchy.f90 d_clement.f90 s_clement.f90 d_chebspec.f90 \
           s_chebspec.f90 d_hilb.f90 s_hilb.f90 
SUBROUTINES = $(SOURCES:.f90=.o)

PYTHON = f_chow.so f_cauchy.so f_hilb.so f_chebspec.so f_clement.so
all: lib 

$(UTILITY_OBJ): $(UTILITY)
	$(FC) $(FFLAGS) -c $^

$(SUBROUTINES): $(SOURCES) 
	$(FC) $(FFLAGS) -c -I ../ $^


lib: $(UTILITY_OBJ) $(SUBROUTINES)
	ar rv matrix_repository.a $^
	ranlib matrix_repository.a
	rm -f *.o 

#makefile for matrix-repository.py 
python: 
	rm -rf matrix-repository.py
	git clone "https://github.com/weijianzhang/matrix-repository.py.git"
	( cd matrix-repository.py ; make )

#makefile for matrix-repository.extension
extension:
	rm -rf matrix-repository.extension
	git clone "https://github.com/weijianzhang/matrix-repository.extension.git"


clean: 
	\rm -f *.o *.a *.mod matrix-repository.py/*.so \
               matrix-repository.py/*.pyc
