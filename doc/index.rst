.. matrix-repository documentation master file, created by
   sphinx-quickstart on Thu Jun 19 18:57:29 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

matrix-repository Documentation
===============================

.. warning::

   matrix-repository will merge with Matrix Depot project shortly.

matrix-repository is a collection of matrices with special properties.
These properties include: known inverses and/or known eigenvalues;
ill-conditioned and/or rank deficient matrices; symmetric, positive
definite, orthogonal, defective, involutory, and/or totally positive.

Contents:

.. toctree::
   :maxdepth: 1
   
   intro
   install
   matrices
   fortran
   python
   julia
   glossary



