==================================
Using matrix-repository in Fortran
==================================

Getting Started
===============

The command::

  $ make 

will create three modules ``mr_type.mod``, ``mr_util.mod`` and
``matrix_repository.mod`` and an archived file ``matrix_repository.a``
in the directory ``matrix-repository``::

  $ ls
  doc       matrix_repository.a          matrix-repository.py  README.rst
  LICENSE   matrix-repository.extension  mr_type.mod           src
  makefile  matrix_repository.mod        mr_util.mod           tests

The archive file contains object files for all matrix subroutines in
the collection. To use matrix-repository, you need to write::

  use matrix_repository, only: matrix_name

at the beginning of your program. Here is an example::

  program test_fortran
    use mr_type, only: WP => DP
    use matrix_repository, only: chow
    implicit none
    real(WP), dimension(5,5) :: A
    integer :: info

  
    call chow(5, A, 5, 1.0_WP, 2.0_WP, info)
  
    write(*,*) "Welcome to Gallery!"
    if (info == 0) then
       write(*,*) "A is a double precision Chow matrix:"
       write(*,"(5 F9.4)") A
    end if
  end program test_fortran

.. note::
   
   In the example above, we also make use of the module ``mr_type``
   which holds the KIND value for different data precision. Here
   ``DP`` stands for double precision.

To compile this example program using gfortran, you need to turn on
the flag ``-I`` to specify the location of the ``.mod`` files that
should be included in the program and link with
``matrix_repository.a``. Suppose the directory ``matrix-repository``
is located at ``~/matrix-repository``, then the command::

 $ gfortran -o test -I ~/matrix-repository test_fortran.f90 \
  ~/matrix-repository/matrix_repository.a

will generate the executable file ``test``. 

.. _utility:

Using Utility Modules
=====================

Two auxiliary modules are ``mr_type`` and ``mr_util``.

Types
-----

The file supplied as ``mr_type.f90`` contains a single module named
``mr_type``, which holds the KIND value and mathematical constants.

============== ============================
Parameters      meaning
============== ============================
``DP``          double precision KIND value
``SP``          single precision KIND value
``PI_DP``       double precision :math:`\pi`
``PI_SP``       single precision :math:`\pi`
============== ============================


Utilities
---------

The file supplied as ``mr_util.f90`` contains a single module named
``mr_util``. The following subroutines or functions are included in
this module.

.. function:: mr_error(string[, info])

   Report an error message include ``string``, if ``info = -i``,
   the error message will tell the ``i``-th argument has an error.

.. function:: mr_outer_product(a, b)

   Perform vector outer product on two given vectors ``a`` and ``b``.

.. function:: d_negative_one_power(i)

   Return the ``i``\ -th power of -1 as double precision.

.. function:: s_negative_one_power(i)

   Return the ``i``\ -th power of -1 as single precision. 

Matrix Subroutines
==================

.. function:: cauchy(n, A, LDA, x, y, info)

   Generate a n-by-n :ref:`cauchy`.

   Parameters
   ----------
   n : integer
      The order of the matrix. Input.
  
   A : (LDA, n) 2-d real array
      A single precision or double precision. Output.

   LDA : integer
      The leading dimension of the matrix A. Input.

   x : (n,) 1-d array
      x single precision or double precision. Input.

   y : (n,) 1-d array
      y single precision or double precision. Input.

   info : integer
      info = 0 if successfully exits; else info = -i then
      i-th argument had all illegal value. Input.

.. function:: clement(n, A, LDA, info)

   Generate a n-by-n :ref:`clement`.

   Parameters
   ----------
   n : integer
     The order of the matrix A. Input.

   A : (LDA, n) 2-d real array
     Clement matrix, single precision or double precision. Output.

   LDA : integer
      The leading dimension of the matrix A. Input.
   
   info : integer
      info = 0 if successfully exits; else info = -i then
      i-th argument had all illegal value. Input.

.. function:: chebspec(n, A, LDA, info)

   Generate a n-by-n :ref:`chebspec`.

   Parameters
   ----------
   n : integer
     The order of the matrix A. Input.

   A : (LDA, n) 2-d real array 
     Chebyshev spectral differentiation matrix, single
     precision or double precision. Output.

   LDA : integer
      The leading dimension of the matrix A. Input.
   
   info : integer
      info = 0 if successfully exits; else info = -i then
      i-th argument had all illegal value. Input.

.. function:: hilb(n, A, LDA, info)

   Generate a n-by-n :ref:`hilbert`.

   Parameters
   ----------
   n : integer
     The order of the matrix A. Input.

   A : (LDA, n) 2-d real array 
     Hilbert matrix, single precision or double precision. 
     Output.

   LDA : integer
      The leading dimension of the matrix A. Input.
   
   info : integer
      info = 0 if successfully exits; else info = -i then
      i-th argument had all illegal value. Input.

.. function:: chow(n, A, LDA, alpha, delta, info)
 
   Generate a n-by-n :ref:`chow`.

   Parameters
   ----------
   n : integer
     The order of the matrix A. Input.

   A : (LDA, n) 2-d real array
     Chow matrix, single precision or double precision. Output.

   LDA : integer
      The leading dimension of the matrix A. Input.

   alpha : real scalar
       single precision or double precision. Input.

   delta : real scalar
       single precision or double precision. Input.
   
   info : integer
      info = 0 if successfully exits; else info = -i then
      i-th argument had all illegal value. Input.
