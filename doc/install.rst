============================
Installing matrix-repository
============================

Dependencies
============

* gfortran compiler 

* To use the Python interface, you need Python 2.6 or above, 
  include Python 3.x, with Numpy v1.8. 

* To use the module ``access``, you need python-pandas.   

Installation
============

To install matrix-repository, type ::

  $ git clone https://github.com/matrix-repository/matrix-repository.git
  
then do::
  
  $ cd matrix-repository
  $ make

To install the Python interface of matrix-repository, type ::

  $ make python
