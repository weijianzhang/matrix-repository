=================================
Using matrix-repository in Python
=================================

When using matrix-repository, we recommend the following import
convention::

  import matrix_repository as mr


Matrix functions
================
.. automodule:: matrix_repository
   :members: 

Access functions
================

.. automodule:: access
   :members:

