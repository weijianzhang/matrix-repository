==========================
Matrices in the Collection
==========================

Notation Convention
===================

We use capital letters, e.g., :math:`A, B, C,` for matrices;
subscripted capital letters, e.g., :math:`A_{ij}` for matrix elements;
lower case letters, e.g., :math:`x, y, z` for vectors and lower case
Greek letters, e.g., :math:`\alpha, \beta, \gamma` for scalars. 

.. _chow:

Chow Matrix
===========

Chow matrix is a singular toeplitz lower hessenberg matrix. The
pattern of a Chow matrix is defined by two parameters, say
:math:`\alpha` and :math:`\delta`. For a :math:`n \times n` matrix, a Chow 
matrix looks like 

.. math::
   :nowrap:
   
   \begin{equation}
   \begin{bmatrix}
    \alpha + \delta & 1 & 0 & \cdots & \cdots & 0 \\
     \alpha^2   & \alpha + \delta & 1  & 0 & \cdots & 0 \\
     \alpha^3  & \alpha^2  & \alpha+\delta & 1 & \cdots & 0 \\
   \vdots & \vdots & \ddots & \ddots & \ddots & \vdots \\
   \vdots & \vdots &        & \ddots & \ddots & 1 \\
    \alpha^n  & \alpha^{n-1} & \cdots & \cdots & \alpha^2 & \alpha+\delta \\
   \end{bmatrix}
   \end{equation}

.. _cauchy:

Cauchy Matrix
=============

Cauchy matrix, named after the French mathematician Augustin-Louis
Cauchy, is a :math:`n \times n` matrix defined by two vectors :math:`x`
and :math:`y` of length :math:`n`, where :math:`x_i - y_i \neq 0`.   
The :math:`(i,j)` element of a Cauchy Matrix :math:`A` is 

.. math::
   :nowrap:

   \begin{equation}
    A_{i,j} = \frac{1}{x_i - y_j}.
   \end{equation}

.. _clement:

Clement Matrix
==============

Clement matrix, named after Paul A. Clement, is a class of
triple-diagonal matrices (should be called Clement matrices) of the 
form

.. math::
   :nowrap:
  
   \begin{equation}
   A = 
   \begin{bmatrix}
   0   & x_1 &     &     &  &    \\
   y_1 & 0   & x_2 &     &  &    \\
       & y_2 & 0   & x_3 &  &     \\
       &     &\ddots&\ddots&\ddots &  \\
       &     &      & \ddots&\ddots & x_n \\
       &     &      &       & y_n   & 0   \\  
   \end{bmatrix}
   \end{equation}.

When :math:`n` is even, :math:`A` is singular.  

.. _chebspec:

Chebyshev Spectral Differentiation Matrix
=========================================

Chebyshev Spectral differentiation matrix :math:`A` is a :math:`n
\times n` matrix with entries 

.. math::
   :nowrap:

   \begin{align}
   A_{1,1} &= \frac{2(n-1)^2 + 1}{6}, & A_{n,n} & = - \frac{2(n-1)^2 + 1}{6}, \\
   A_{j,j} &= \frac{-x_j}{2(1 - x_j^2)}, &  j  & = 2,\ldots, n-1, \\
   A_{i,j} &= \frac{c_i (-1)^{i+j}}{c_j(x_i - x_j)}, & i &\neq j, \\
   \end{align}

where 

.. math::
   :nowrap:
   
   \begin{equation}
   c_i = \begin{cases}
         2 & i = 1 \text{ or } n, \\
         1 & \text{otherwise},
        \end{cases}
   \end{equation}

and :math:`x_j = \cos(j\pi/n)` for :math:`j = 1, \ldots, n` are the
Chebyshev points.


.. _hilbert:

Hilbert Matrix
==============

Hilbert matrix, named after the German mathematician David Hilbert is
a special case of :ref:`cauchy`. The :math:`(i,j)` element of a
Hilbert matrix :math:`A` is 

.. math::
   :nowrap:
  
   \begin{equation}
   A_{i,j} = \frac{1}{i + j - 1}.
   \end{equation}


