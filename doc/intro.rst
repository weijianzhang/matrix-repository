============
Introduction
============

Background
==========

Test matrices are born with the first computer programs for matrix
computations in the 1940s. Test matrix collections have been
developed ever since. The Harwell-Boeing Collection developed by Duff,
Laboratory, Grimes and Lewis in 1989 is a set of test matrices for
sparse matrix problems. The problems are drawn from a wide variety of
scientific and engineering disciplines.  In 1991, Higham present a
collection of 45 parametrized test matrices in MATLAB. The matrices
are mostly square, dense, nonrandom, and of arbitrary
dimension. Making use of the World Wide Web, Boisvert et al. develop a
repository of test matrices (Matrix Market) which incorporated 
the Harwell-Boeing collection. The University of Florida Sparse Matrix
Collection is another large set of sparse matrices that arise in real
applications.

In modern days, mixed languages programming is a common practice for
developing large projects: using a high-level language for coding the
big picture and a low-level language for numerically intensive
elements. Similarly, Numerical analysts would first test an algorithm
on MATLAB or Python and then deploy the algorithm to Fortran or C. If,
however, the test matrices in different languages are coded
differently, the results and error generated may be different, which
is problematic to analyse.  The matrix collections mentioned above
either contained fixed dimension matrices or were coded for a specific
language. Our aim is to provide a test matrix collection that can
be used in a variety languages and contains matrices of arbitrary
dimension.

License
=======

The MIT License (MIT)

Copyright (c) 2014 Weijian Zhang

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Acknowledgements
================

I would like to thank my advisor Nicholas J. Higham for 
his recommendations, supports, comments and useful feedback on 
early drafts of this software.
I would like to thank Edvin Deadman for checking my early drafts
and providing many useful suggestions. I would also like to acknowledge
suggestions and help from Sven Hammarling, Philip E. Gill and 
Mario Berljafa. 
