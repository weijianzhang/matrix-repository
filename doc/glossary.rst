========
Glossary
========

.. glossary::
   :sorted:

   Involutory 
      An involutory matrix is a matrix that is its own inverse, 
      i.e., :math:`A^2 =I` for a matrix :math:`A`.

   Defective
      A defective matrix is a square matrix that does not have a 
      complete basis of eignvectors.

   Hessenberg
      An upper Hessenberg matrix has zero entries below the first
      subdiagonal and a lower Hessenberg matrix has zero entries 
      above the first superdiagonal.

   Toeplitz
      A Toeplitz matrix has equal entries on each diagonal and it 
      looks like

       .. math::
            :nowrap:
       
            \begin{equation}
            \begin{bmatrix}
            t_0 & t_1 & t_2 & \cdots & \cdots &t_{n-1}\\
            t_{-1}& t_0 & t_1 & t_2 & \cdots & t_{n-2} \\
            t_{-2}& t_{-1}&t_0 & t_1 &  \cdots & t_{n-3} \\
            \vdots &\vdots & \ddots & \ddots & \ddots & \vdots \\
            \vdots & \vdots &        & \ddots & \ddots & t_1 \\
             t_{-n+1} &t_{-n+2}& \cdots & \cdots & t_{-1} & t_0 \\
            \end{bmatrix}.
            \end{equation}
         
     
   Hankel 
      A Hankel matrix has equal entries on each anti-diagonal.
  
