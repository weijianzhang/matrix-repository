program test_fortran
  use mr_type, only: WP => DP
  use matrix_repository, only: chow
  implicit none
  real(WP), dimension(5,5) :: A
  integer :: info

  
  call chow(5, A, 5, 1.0_WP, 2.0_WP, info)
  
  write(*,*) "Welcome to Gallery!"
  if (info == 0) then
     write(*,*) "A is a double precision Chow matrix:"
     write(*,"(5 F9.4)") A
  end if
  
end program test_fortran
