import numpy as np
import f_chow as fortran_chow
import f_hilb as fortran_hilb
import f_cauchy as fortran_cauchy
import f_chebspec as fortran_chebspec
import f_clement as fortran_clement

__all__=['chow','hilb', 'cauchy', 'chebspec', 'clement']

def chow(n, alpha = 1, delta = 0, dtype=None):
    """Construct :ref:`chow`.
    
    chow(n, alpha, delta) is the Chow matrix - a singular Toeplitz lower
    Hessenberg matrix. A = chow(n, alpha, delta) is the matrix A =
    H(alpha) + delta*eye, where H is the lower Hessenberg matrix
    with H(i,j) = alpha^(i-j+1).  Default: alpha = 1, delta = 0.

    Parameters
    ----------
    n : integer
        The dimension of the matrix.
    alpha : integer or floating point
    
    delta : integer or floating point
    
    dtype : dtype
            Data type of the matrix. 

    Returns
    -------
    chow : (n, n) ndarray
           Chow matrix.

    Examples
    --------
    >>> from matrix_repository import chow
    >>> chow(4, dtype=int)
    array([[1, 1, 0, 0],
           [1, 1, 1, 0],
           [1, 1, 1, 1],
           [1, 1, 1, 1]])
                       
    """
    a, info = fortran_chow.d_chow(n, n, alpha, delta)

    if not info == 0:
        raise ValueError("Fortran kernel error.")

    if dtype is None:
        return a
    else:
        return a.astype(dtype)


def hilb(n, dtype=None):
    """Construct :ref:`hilbert`.
    
    hilb(N) is the N-by-N matrix with elements 1/(i+j-1).
    It is a famous example of a badly conditioned matrix.

    Parameters
    ----------
    n : integer
        The dimension of the matrix.
        
    dtype : dtype
        Data type of the matrix.

    Returns
    -------
    hilb : (n, n) ndarray
        Hilbert matrix.

    Examples
    --------
    >>> from matrix_repository import hilb
    >>> hilb(5)
    array([[ 1.        ,  0.5       ,  0.33333333,  0.25      ,  0.2       ],
           [ 0.5       ,  0.33333333,  0.25      ,  0.2       ,  0.16666667],
           [ 0.33333333,  0.25      ,  0.2       ,  0.16666667,  0.14285714],
           [ 0.25      ,  0.2       ,  0.16666667,  0.14285714,  0.125     ],
           [ 0.2       ,  0.16666667,  0.14285714,  0.125     ,  0.11111111]])
           
    """
    if n < 1:
        raise ValueError("Matrix size must be one or greater.")
    elif n == 1:
        return np.array([[1]])

    a, info = fortran_hilb.d_hilb(n,n)

    if not info == 0:
        raise ValueError("Illegal arguments in Fortran kernel.")

    if dtype is None:
        return a
    else:
        return a.astype(dtype)
    

def cauchy(x, y = None, dtype = None):
    """Construct :ref:`cauchy`
    
    cauchy(x, y), where x, y are n-vectors, is the n-by-n matrix
    with c(i,j) = 1/(x(i)+y(j)).   By default, y = x.
    Special case: if x is a scalar cauchy(x) is the same as cauchy(1:x).

    Parameters
    ----------
    x : (n,) array_like
        1-D array or a scalar.
        
    y : (n,) array_like
        1-D array or a scalar.

    dtype : dtype
        Data type of the matrix.


    Returns
    -------
    cauchy : (n, n) ndarray
        Cauchy matrix.

    Examples
    --------
    >>> from numpy import array
    >>> from matrix_repository import cauchy
    >>> cauchy(array([1,2,3]))
    array([[ 0.5       ,  0.33333333,  0.25      ],
           [ 0.33333333,  0.25      ,  0.2       ],
           [ 0.25      ,  0.2       ,  0.16666667]])
    >>> cauchy(4)
    array([[ 0.5       ,  0.33333333,  0.25      ,  0.2       ],
           [ 0.33333333,  0.25      ,  0.2       ,  0.16666667],
           [ 0.25      ,  0.2       ,  0.16666667,  0.14285714],
           [ 0.2       ,  0.16666667,  0.14285714,  0.125     ]])
           
    """
    try:
        n, = x.shape
    except AttributeError:
        n = x
        x = np.arange(1, n + 1)
            
    if y is None:
        y = x

    if not x.shape == y.shape:
        raise ValueError("Parameter vectors must be of same dimension.")

    n = x.shape[0]
    a, info = fortran_cauchy.d_cauchy(n, n, x, y)
        
    if not info == 0:
        raise ValueError("Fortran kernel error.")
        
    if dtype is None:
        return a
    else:
        return a.astype(dtype)
   

def chebspec(n, dtype=None):
    """Construct :ref:`chebspec`
    
    c = chebspec(n) is a Chebyshev spectral differentiation
    matrix of order n.  
    The computed eigenvector matrix x from eig is
    ill-conditioned (mesh(real(x)) is interesting).

    Parameters
    ----------
    n : integer
        The dimension of the matrix.

    dtype : dtype
        Data type of the matrix.

    Returns
    -------
    chebspec : (n, n) ndarray
        Chebyshev spectral differentiation matrix.

    Examples
    --------
    >>> from matrix_repository import chebspec
    >>> chebspec(4)
    array([[ 3.16666667, -4.        ,  1.33333333, -0.5       ],
           [ 1.        , -0.33333333, -1.        ,  0.33333333],
           [-0.33333333,  1.        ,  0.33333333, -1.        ],
           [ 0.5       , -1.33333333,  4.        , -3.16666667]])
    
    """

    a, info = fortran_chebspec.d_chebspec(n,n)

    if not info == 0:
        raise ValueError("Fortran kernel error.")

    if dtype is None:
        return a
    else:
        return a.astype(dtype)

def clement(n, dtype=None):
    """Construct :ref:`clement`
    
    clement(n) is a tridiagonal matrix with zero diagonal entries
    and known eigenvalues.  It is singular if N is odd.  About 64
    percent of the entries of the inverse are zero.  The eigenvalues
    are plus and minus the numbers N-1, N-3, N-5, ..., (1 or 0).

    Parameters
    ----------
    n : integer
       The dimension of the matrix.

    dtype : dtype
       Data type of the matrix.

    Returns
    -------
    clement : (n, n) ndarray
          Clement matrix.

    Examples
    --------
    >>> from matrix_repository import clement
    >>> clement(4)
    array([[ 0.,  1.,  0.,  0.],
           [ 3.,  0.,  2.,  0.],
           [ 0.,  2.,  0.,  3.],
           [ 0.,  0.,  1.,  0.]])
           
    """
    a, info = fortran_clement.d_clement(n,n)

    if not info == 0:
        raise ValueError("Fortran kernel error.")

    if dtype is None:
        return a
    else:
        return a.astype(dtype)
    
