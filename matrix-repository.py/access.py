from __future__ import print_function
import numpy as np
import pandas as pd
from matrix_repository import *


__all__=['matrix','property']

def matrix(index, size=None, dtype=None):
    """Access matrices by number.

    matrix(index, size): access matrices in the collection
    using the integer index. The size of the matrix is
    defined by the integer size.

    Parameters
    ----------
    index : integer
          The index of all the matrices in the collection.
          use matrix(help) to see.

    size : integer
        The dimension of the matrix.

    dtype : dtype
        Data type of the matrix.
            
    Returns
    -------
    matrix : (n, n) ndarray

    Examples
    --------
    >>> from access import matrix
    >>> matrix(help)
    0 : chow 1 : hilb 2 : cauchy 3 : chebspec 4 : clement
    >>> matrix(1)
    hilb
    >>> matrix(1,5)
    array([[ 1.        ,  0.5       ,  0.33333333,  0.25      ,  0.2       ],
           [ 0.5       ,  0.33333333,  0.25      ,  0.2       ,  0.16666667],
           [ 0.33333333,  0.25      ,  0.2       ,  0.16666667,  0.14285714],
           [ 0.25      ,  0.2       ,  0.16666667,  0.14285714,  0.125     ],
           [ 0.2       ,  0.16666667,  0.14285714,  0.125     ,  0.11111111]])
    
    """
    matrices = [chow, hilb, cauchy, chebspec, clement]
        
    try:
        matrices[index]
    except TypeError:
        n = len(matrices)
        for i in range(n):
            print (i, ":", matrices[i].__name__, end=" ")
        print ("\n")
        return
        
    if size is None:
        print (matrices[index].__name__)
    else:
        if dtype is None:
            return matrices[index](size)
        else:
            return matrices[index](size, dtype=dtype)

        
def property(matrix_name = None, *argv):
    """ Print the property of a given matrix.

    property(matrix_name): if matrix_name is given, then the properties
    of that matrix are printed out. If no argument is given, then the
    properties of all matrices are printed out.
    
    1: yes, it has that property. 0: no, it doesn't have that property.
    
    Inverse: the inverse is known explicitly.
    
    Ill-cond: ill-conditioned for some values of arguments.
    
    Rank: rank-deficient for some value of arguments.
    
    Symm: symmetric for some values of arguments.
    
    Pos Def: symmetric positive definite for some values of the parameters.
    
    Orth: orthogonal or a diagonal scaling of an orthognal matrix, for
    some values of the parameters.
          
    Eig: something is known about the eignsystem.

    Parameters
    ----------
    matrix_name : str
               matrix name.

    Returns
    -------
    property : pandas DataFrame
            list of properties

    Examples
    --------
    >>> from access import property
    >>> property('chow')
    Inverse     0
    Ill-cond    0
    Rank        1
    Symm        0
    Pos Def     0
    Orth        0
    Eig         1
    >>> property()
              Inverse  Ill-cond  Rank  Symm  Pos Def  Orth  Eig
    cauchy          1         1     0     1        1     0    0
    chebspec        0         0     1     0        0     0    1
    chow            0         0     1     0        0     0    1
    clement         1         0     1     1        0     0    1
    hilb            1         1     0     1        1     0    0

    """
    data = {'chow':    [ 0, 0, 1, 0, 0, 0, 1],
            'hilb':    [ 1, 1, 0, 1, 1, 0, 0],
            'cauchy':  [ 1, 1, 0, 1, 1, 0, 0],
            'chebspec':[ 0, 0, 1, 0 ,0 ,0, 1],
            'clement': [ 1, 0, 1, 1, 0, 0, 1]}
        
    props = ['Inverse', 'Ill-cond', 'Rank', 'Symm', 'Pos Def', 'Orth',
            'Eig']
    
    dataFrame = pd.DataFrame(data, index= props)
            
    if matrix_name is None:
        return dataFrame.T
    else:
        return dataFrame[matrix_name]

    
    
    

