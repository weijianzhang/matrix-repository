module mr_type
  implicit none
  integer, parameter :: DP = kind(1.0D0) !Double precision
  integer, parameter :: SP = kind(1.0)   !Single precision

  !Mathematical constant pi (DP and SP version)
  real(DP), parameter :: PI_DP = 4.0E0_DP * atan(1.0E0_DP) 
  real(SP), parameter :: PI_SP = 4.0E0_SP * atan(1.0E0_SP)
end module mr_type
