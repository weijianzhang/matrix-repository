module mr_util
  use mr_type, only: DP, SP
  implicit none
  private
  public mr_error, mr_outer_product, d_negative_one_power, &
  &       s_negative_one_power

  interface mr_error
     module procedure mr_error_info, mr_error_str
  end interface mr_error

  interface mr_outer_product
     module procedure d_outer_product, s_outer_product
  end interface mr_outer_product

contains
  !
  !mr_error_info
  !-------------
  !Print error message indicating the illegal argument and die.
  !
  subroutine mr_error_info(string, info)
    character(len = *), intent(in) :: string
    integer, intent(in) :: info
    print *, "matrix-repository error: "
    print *, string, ", an illegal value at parameter", info
    stop "Program terminated by mr_error." 
  end subroutine mr_error_info
  ! 
  !mr_error_str
  !------------
  !Print the error message and die.
  !
  subroutine mr_error_str(string)
    character(len = *), intent(in) :: string
    print *, "matrix-repository error"
    print *, string
    stop "Program terminated by mr_error."
  end subroutine mr_error_str
  !
  !d_outer_product
  !---------------
  !Perform vector outer product on two given vectors a and b
  !Double precision version
  !
  function d_outer_product(a,b)
    use mr_type, only: WP => DP
    implicit none
    real(WP), dimension(:), intent(in) :: a,b
    real(WP), dimension(size(a), size(b)) :: d_outer_product
    d_outer_product = spread(a, dim = 2, ncopies = size(b)) * &
         & spread(b, dim = 1, ncopies = size(a))
  end function d_outer_product
  !
  !s_outer_product
  !---------------
  !Perform vector outer product on two given vectors a and b
  !Single precision version
  !
  function s_outer_product(a,b)
    use mr_type, only: WP => SP
    implicit none
    real(WP), dimension(:), intent(in) :: a,b
    real(WP), dimension(size(a), size(b)) :: s_outer_product
    s_outer_product = spread(a, dim = 2, ncopies = size(b)) * &
         & spread(b, dim = 1, ncopies = size(a))
  end function s_outer_product
  !
  !d_negative_one_power
  !--------------------
  !Return the i-th power of -1 as double precision.
  !
  function d_negative_one_power(i)
    use mr_type, only: WP => DP
    implicit none
    integer :: i
    real(WP) :: d_negative_one_power

    if (mod (i, 2) == 0) then
       d_negative_one_power = 1.0_WP
    else
       d_negative_one_power = -1.0_WP
    end if
  end function d_negative_one_power
  !
  !s_negative_one_power
  !--------------------
  !Return the i-th power of -1 as single precision.
  !
  function s_negative_one_power(i)
    use mr_type, only: WP => SP
    implicit none
    integer :: i
    real(WP) :: s_negative_one_power

    if (mod (i, 2) == 0) then
       s_negative_one_power = 1.0_WP
    else
       s_negative_one_power = -1.0_WP
    end if
  end function s_negative_one_power
 
end module mr_util
