subroutine d_chebspec(n, A, LDA, info)
  ! Generate a LDA-by-n :ref:`chebspec`.

  ! Parameters
  ! ----------
  ! n : integer
  !   The order of the matrix A. Input.

  ! A : (LDA, n) 2-d array 
  !   Chebyshev spectral differentiation matrix, single
  !   precision or double precision. Output.

  ! LDA : integer
  !    The leading dimension of the matrix A. Input.

  ! info : integer
  !    info = 0 if successfully exits; else info = -i then
  !    i-th argument had all illegal value. Input.

  use mr_type, only: WP => DP, PI => PI_DP
  use mr_util, only: mr_error, negative_one_power => d_negative_one_power
  implicit none
  integer, intent(in) :: n, LDA
  real(WP), dimension(LDA, n), intent(out) :: A
  integer, intent(out) :: info
  !Local variables
  integer :: i, j, allocate_status, deallocate_status
  real(WP), dimension(:), allocatable :: c, x 
  !Check the arguments
  info = 0
  if (n < 0) then
     info = -1
  else if (LDA < n) then
     info = -3
  end if
  if (info /= 0) then
     call mr_error("chebspec", -info)
  end if

  if (n == 1) then
     A(1,1) = 1.0_WP
     return
  end if

  allocate(c(n), stat = allocate_status)
  if (allocate_status /= 0) then
     call mr_error("chebspec: allocate local variable c unsuccessfully.")
  end if

  c(1) = 2.0_WP
  c(2:n-1) = 1.0_WP
  c(n) = 2.0_WP

  allocate(x(n), stat = allocate_status)
  if (allocate_status /= 0) then
     call mr_error("chebspec: allocate local variable x unsuccessfully.")
  end if
  !Compute the Chebyshev points.
  do i = 1,n
     x(i) = cos (PI * real( i - 1, WP) / real( n - 1, WP))
  end do

  do j = 1,n
     do i = 1,n

        if (i /= j) then
           A(i,j) = negative_one_power(i + j) * c(i) / &
                & ( c(j) * (x(i) - x(j)) )
        else if (i == 1) then
           A(i,i) = real( 2 * (n - 1) * (n - 1) + 1, WP) / 6.0_WP
        else if (i == n) then
           A(i,i) = - real( 2 *(n - 1) * (n - 1) + 1, WP) / 6.0_WP
        else
           A(i,i) = -0.5_WP * x(i) / (1.0_WP - x(i) * x(i))
        end if

     end do
  end do

  deallocate(c, stat = deallocate_status)
  if (deallocate_status /= 0) then
     call mr_error("chebspec: deallocate local variable c unsuccessfully.")
  end if

  deallocate(x, stat = deallocate_status)
  if (deallocate_status /= 0) then
     call mr_error("chebspec: deallocate local variable x unsuccessfully.")
  end if

end subroutine d_chebspec



