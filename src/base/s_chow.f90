subroutine s_chow(n, A, LDA, alpha, delta, info)
  ! Generate a LDA-by-n :ref:`chow`.

  ! Parameters
  ! ----------
  ! n : integer
  !   The order of the matrix A. Input.

  ! A : (LDA, n) 2-d real array
  !   Chow matrix, single precision or double precision. Output.

  ! LDA : integer
  !    The leading dimension of the matrix A. Input.

  ! alpha : real scalar
  !     single precision or double precision. Input.

  ! delta : real scalar
  !     single precision or double precision. Input.

  ! info : integer
  !    info = 0 if successfully exits; else info = -i then
  !    i-th argument had all illegal value. Input.
  use mr_type, only: WP => SP
  use mr_util, only: mr_error
  implicit none
  integer, intent(in) :: n, LDA
  real(WP), dimension(LDA,n), intent(out) :: A
  real(WP), intent(in) :: alpha, delta
  integer, intent(out) :: info
  !local variable
  integer :: i, j
  ! check the arguments
  info = 0
  if (n < 0) then
     info = -1
  else if (LDA < n) then
     info = -3
  end if
  if (info /= 0) then
     call mr_error("chow", -info)
     return
  end if

  do j = 1, n
     do i = 1, n
        if(i == j - 1) then
           A(i,j) = 1.0_WP
        else if (i == j) then
           A(i,j) = alpha + delta
        else if (j + 1 <= i) then
           A(i,j) = alpha**(i + 1 -j)
        else
           A(i,j) = 0.0_WP
        end if
     end do
  end do
end subroutine s_chow


