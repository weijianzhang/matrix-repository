subroutine s_clement(n, A, LDA, info)
  ! Generate a LDA-by-n :ref:`clement`.

  ! Parameters
  ! ----------
  ! n : integer
  !   The order of the matrix A. Input.

  ! A : (LDA, n) 2-d array
  !   Clement matrix, single precision or double precision. Output.

  ! LDA : integer
  !    The leading dimension of the matrix A. Input.

  ! info : integer
  !    info = 0 if successfully exits; else info = -i then
  !    i-th argument had all illegal value. Input.
  use mr_type, only: WP => SP
  use mr_util, only: mr_error
  implicit none
  integer, intent(in) :: n, LDA
  real(WP), dimension(LDA,n), intent(out) :: A
  integer, intent(out) :: info
  !Local variables
  integer :: i,j
  !Check the arguments
  info = 0
  if (n < 0) then
     info = -1
  else if (LDA < n) then
     info = -3
  end if
  if (info /= 0) then
     call mr_error("clement", -info)
  end if

  A(1:n, 1:n) = 0.0_WP

  do j = 1, n
     do i = 1, n
        if (j == i + 1) then
           A(i,j) = real (i, WP)
        else if (j == i -1) then
           A(i,j) = real (n - j, WP)
        end if

     end do
  end do
end subroutine s_clement

