module matrix_repository
  private
  public cauchy, chebspec, chow, clement, hilb
  
  interface cauchy
     subroutine d_cauchy(n, A, LDA, x, y, info)
       use mr_type, only: WP => DP
       implicit none
       integer, intent(in) :: n, LDA 
       real(WP), dimension(LDA,n), intent(out) :: A
       real(WP), dimension(n), intent(in) :: x
       real(WP), dimension(n), intent(in) :: y
       integer, intent(out) :: info
     end subroutine d_cauchy
     subroutine s_cauchy(n, A, LDA, x, y, info)
       use mr_type, only: WP => SP
       implicit none
       integer, intent(in) :: n, LDA 
       real(WP), dimension(LDA,n), intent(out) :: A
       real(WP), dimension(n), intent(in) :: x
       real(WP), dimension(n), intent(in) :: y
       integer, intent(out) :: info
     end subroutine s_cauchy
  end interface cauchy


  interface chebspec
     subroutine d_chebspec(n, A, LDA, info)
       use mr_type, only: WP => DP
       implicit none
       integer, intent(in) :: n, LDA
       real(WP), dimension(LDA, n) :: A
       integer, intent(out) :: info
     end subroutine d_chebspec
     subroutine s_chebspec(n, A, LDA, info)
       use mr_type, only: WP => SP
       implicit none
       integer, intent(in) :: n, LDA
       real(WP), dimension(LDA, n) :: A
       integer, intent(out) :: info
     end subroutine s_chebspec
  end interface chebspec

  interface chow
     subroutine d_chow(n, A, LDA, alpha, delta, info)
       use mr_type, only: WP => DP
       implicit none
       integer, intent(in) :: n, LDA
       real(WP), dimension(LDA,n), intent(out) :: A
       real(WP), intent(in) :: alpha, delta
       integer, intent(out) :: info
     end subroutine d_chow
     subroutine s_chow(n, A, LDA, alpha, delta, info)
       use mr_type, only: WP => SP
       implicit none
       integer, intent(in) :: n, LDA
       real(WP), dimension(LDA,n), intent(out) :: A
       real(WP), intent(in) :: alpha, delta
       integer, intent(out) :: info
     end subroutine s_chow
  end interface chow

  interface clement
     subroutine d_clement(n, A, LDA, info)
       use mr_type, only: WP => DP
       implicit none
       integer, intent(in) :: n, LDA
       real(WP), dimension(LDA, n), intent(out) :: A
       integer, intent(out) :: info
     end subroutine d_clement

     subroutine s_clement(n, A, LDA, info)
       use mr_type, only: WP => SP
       implicit none
       integer, intent(in) :: n, LDA
       real(WP), dimension(LDA, n), intent(out) :: A
       integer, intent(out) :: info
     end subroutine s_clement
  end interface clement

  interface hilb
     subroutine d_hilb(n, A, LDA, info)
       use mr_type, only: WP => DP
       implicit none
       integer, intent(in) :: n
       real(WP), dimension(LDA,n), intent(out) :: A
       integer, intent(in) :: LDA
       integer, intent(out) :: info
     end subroutine d_hilb

     subroutine s_hilb(n, A, LDA, info)
       use mr_type, only: WP => SP
       implicit none
       integer, intent(in) :: n
       real(WP), dimension(LDA,n), intent(out) :: A
       integer, intent(in) :: LDA
       integer, intent(out) :: info
     end subroutine s_hilb
  end interface hilb

end module matrix_repository
