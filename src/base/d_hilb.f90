subroutine d_hilb(n, A, LDA, info)
  ! Generate a LDA-by-n :ref:`hilbert`.

  ! Parameters
  ! ----------
  ! n : integer
  !   The order of the matrix A. Input.

  ! A : (LDA, n) 2-d array 
  !   Hilbert matrix, single precision or double precision. 
  !   Output.

  ! LDA : integer
  !    The leading dimension of the matrix A. Input.

  ! info : integer
  !    info = 0 if successfully exits; else info = -i then
  !    i-th argument had all illegal value. Input.

  use mr_type, only: WP => DP
  use mr_util, only: mr_error
  implicit none
  integer, intent(in) :: n
  real(WP), dimension(LDA,n), intent(out) :: A
  integer, intent(in) :: LDA
  integer, intent(out) :: info
  !Local variables
  integer :: i,j
  !Check the arguments
  info = 0
  if(n < 0) then
     info = -1
  else if(LDA < n) then
     info = -3
  end if
  if(info /=  0) then
     call mr_error("hilb", -info)
     return
  end if


  do j = 1, n
     do i = 1, n
        A(i,j) = 1.0_WP / real( i + j -1, WP)
     end do
  end do
end subroutine d_hilb


