subroutine s_cauchy(n, A, LDA, x, y, info)
  ! Generate a LDA-by-n :ref:`cauchy`.

  !   Parameters
  !   ----------
  !   n : integer
  !      The order of the matrix. Input.

  !   A : (LDA, n) 2-d array
  !      A single precision or double precision. Output.

  !   LDA : integer
  !      The leading dimension of the matrix A.

  !   x : (n,) 1-d array
  !      x single precision or double precision. Input.

  !   y : (n,) 1-d array
  !      y single precision or double precision. Input.

  !   info : integer
  !      info = 0 if successfully exits; else info = -i then
  !      i-th argument had all illegal value. Input.

  use mr_type, only: WP => SP  
  use mr_util
  implicit none
  integer, intent(in) :: n, LDA 
  real(WP), dimension(LDA,n), intent(out) :: A
  real(WP), dimension(n), intent(in) :: x
  real(WP), dimension(n), intent(in) :: y
  integer, intent(out) :: info
  !Local variables
  integer :: i,j
  !Check the arguments
  info = 0
  if (n < 0) then
     info = -1
  else if(LDA < n) then
     info = -3
  end if
  if (info .NE. 0) then
     call mr_error("cauchy", -info)
     return
  end if

  do j = 1,n
     do i = 1,n
        A(i,j) = 1.0_WP/( x(i) + y(j) )
     end do
  end do
end subroutine s_cauchy
